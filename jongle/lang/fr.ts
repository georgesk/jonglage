<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Ui_main.py" line="218"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="226"/>
        <source>Start/Stop [Space bar]</source>
        <translation>Démarrage/arrêt [Barre d&apos;espace]</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="227"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="220"/>
        <source>Rerun from beginning [Home]</source>
        <translation>Relancer depuis le début [Début]</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="221"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="232"/>
        <source>Launch simulation with the programs [F9]</source>
        <translation>Lancer la simulation avec les programmes [F9]</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="233"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="234"/>
        <source>Operation</source>
        <translation>Opération</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="235"/>
        <source>Simulated scene</source>
        <translation>Scène simulée</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="236"/>
        <source>Save &amp;As</source>
        <translation>Enregistrer so&amp;us</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="255"/>
        <source>Check Synta&amp;x</source>
        <translation>Vérifier la synta&amp;xe</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="238"/>
        <source>Programs</source>
        <translation>Programmes</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="241"/>
        <source>Fi&amp;le</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="242"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="243"/>
        <source>P&amp;rograms</source>
        <translation>&amp;Programmes</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="244"/>
        <source>&amp;Open Scene ...</source>
        <translation>&amp;Ouvrir une scène ...</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="245"/>
        <source>&amp;New Scene</source>
        <translation>&amp;Nouvelle scène</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="246"/>
        <source>Import S&amp;VG ...</source>
        <translation>Importer S&amp;VG ...</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="247"/>
        <source>&amp;Initial velocities</source>
        <translation>Vitesses &amp;initiales</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="248"/>
        <source>S&amp;cale</source>
        <translation>É&amp;chelle</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="249"/>
        <source>&amp;G ...</source>
        <translation>&amp;G ...</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="250"/>
        <source>&amp;About ...</source>
        <translation>&amp;A propos ...</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="251"/>
        <source>&amp;Help (F1)</source>
        <translation>&amp;Aide (F1)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="252"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation>&amp;Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="253"/>
        <source>&amp;Save standalone</source>
        <translation>Enregi&amp;strer tout seul</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="254"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../main.py" line="517"/>
        <source>video t = </source>
        <translation>vidéo t = </translation>
    </message>
    <message>
        <location filename="../main.py" line="422"/>
        <source>The simulation is finished</source>
        <translation>La simulation est finie</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="224"/>
        <source>Physical objects (check to move)</source>
        <translation type="obsolete">Objets physiques (cocher pour déplacer)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="222"/>
        <source>One image back</source>
        <translation>Une image plus tôt</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="229"/>
        <source>One image forward</source>
        <translation>UNe image plus tard</translation>
    </message>
    <message>
        <location filename="../main.py" line="245"/>
        <source>File to save the programs</source>
        <translation>Fichier pour enregistrer les programmes</translation>
    </message>
    <message>
        <location filename="../main.py" line="260"/>
        <source>Programs ({p})</source>
        <translation>Programmes ({p})</translation>
    </message>
    <message>
        <location filename="../main.py" line="287"/>
        <source>Open a file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../main.py" line="595"/>
        <source>Indentation error</source>
        <translation>Erreur d&apos;indentation</translation>
    </message>
    <message>
        <location filename="../main.py" line="605"/>
        <source>line # {l} column # {c}
{t}
</source>
        <translation>ligne {l}, colonne {c}
{t}
</translation>
    </message>
    <message>
        <location filename="../main.py" line="605"/>
        <source>Syntax error</source>
        <translation>Erreur de syntaxe</translation>
    </message>
    <message>
        <location filename="../main.py" line="614"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../main.py" line="625"/>
        <source>Compilation successful</source>
        <translation>Compilation réussie</translation>
    </message>
    <message>
        <location filename="../main.py" line="625"/>
        <source>Defined functions:
</source>
        <translation>Fonctions définies :
</translation>
    </message>
    <message>
        <location filename="../main.py" line="517"/>
        <source>(# %d)</source>
        <translation>(N° %d)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="223"/>
        <source>One image back (-)</source>
        <translation>Une image plus tôt (-)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="224"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="228"/>
        <source>One image forward (+)</source>
        <translation>Une image plus tard (+)</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="230"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="239"/>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="../Ui_main.py" line="240"/>
        <source>Physical objects (can be moved when checked)</source>
        <translation>Objets physiques (déplaçables quand on les coche)</translation>
    </message>
    <message>
        <location filename="../main.py" line="205"/>
        <source>Use</source>
        <translation>Usage</translation>
    </message>
    <message>
        <location filename="../main.py" line="205"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../main.py" line="205"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../main.py" line="205"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../main.py" line="205"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
</context>
</TS>
